<?php

/**
 * @file
 * Contains functions handling views integration.
 */

/**
 * Return CCK Views data for the ctt_field_settings($op == 'views data').
 */
function ctt_views_content_field_data($field) {
  // Build the automatic views data provided for us by CCK.
  // This creates all the information necessary for the "ctt" field.
  $data = content_views_field_views_data($field);

  $db_info = content_database_info($field);
  $table_alias = content_views_tablename($field);
  $field_types = _content_field_types();

  return $data;
}
