Collapsed Text and Textarea
---------------------------

Content Text and text area is a composite collapsible field for CCK (Content
Construction Kit). This field is collapsed by default.

Primary use was to built feature lists but you can change sub-fields labels as
you like. For example, it could be used to build collapsed :
- Definition lists (dl)
- FAQ (Frequently Asked Questions)
- Criteria

Advanced users may use Collapse Text which is an input filter available in a
textarea field. CTT was built to avoid mistakes made by beginners using
Collapse Text. http://drupal.org/project/collapse_text

Please test and add your feedbacks in the issue queue.

6.x-1.x supports Views 2 since beta2.

Installation
------------

To install, place the entire CTT folder into your modules directory.
Go to Administer -> Site building -> Modules and enable the Collapsed Text 
Textarea module and Content module (CCK).

Now go to Administer -> Content management -> Content types. Create a new
content type and edit it to add a Collapsed Text and Textarea field. Then test
by creating a new node of your new type using the Create content menu link.

A comprehensive guide to using CCK is available as a CCK Handbook
at http://drupal.org/node/101723.

Maintainers
-----------
The Collapsed Text and Textarea was developped by Leon Cros.
